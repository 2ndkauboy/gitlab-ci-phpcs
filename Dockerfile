FROM composer

ENV PATH "/app/vendor/bin:$PATH"

RUN composer init --name="2ndkauboy/gitlab-ci-phpcs" && \
    composer config --no-plugins allow-plugins.dealerdirect/phpcodesniffer-composer-installer true && \
    composer require squizlabs/php_codesniffer:3.6.* && \
    composer require dealerdirect/phpcodesniffer-composer-installer
